int i = 0, wind = 0;

void setup() {
  Serial.begin(9600);
}

void loop() {
  // read the input on analog pin A0:
  wind = wind + analogRead(A0);
  Serial.println(wind);
  delay(200);

  i = (i+1)%5;
  if(i == 0){
    wind = (wind/5)*1.25;
    Serial.print("Vitesse du vent : "); // print wind speed 
    Serial.println(wind);
    wind = 0;
  }
}
