Anémomètre
====

Ou comment créer un anémomètre avec un vieux ventilateur d'ordinateur et une carte arduino.  

Ce projet a été imaginé et réalisé par Valentin Rosset et Corentin Bettiol lors du cours Fablab Jam Session dispensé par M. Joel Chevrier fin 2018.

[Présentation du projet sur le site du fablab de la casemate](https://fablab.lacasemate.fr/#!/projects/anemometre-maison).